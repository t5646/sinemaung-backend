from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response

# Create your views here.

# API Overview
@api_view(['GET'])
def apiOverview(request):
    api_urls = {
        'Film':'film/',
        'Bioskop':'bioskop/',
        'Jadwal':'jadwal/',
        'Makanan':'makanan/',
    }

    return Response(api_urls)