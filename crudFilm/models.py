from django.db import models

# Create your models here.

class Genre(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name

class Film(models.Model):
    title = models.CharField(max_length=200)
    desc = models.CharField(max_length=1000, null=True)
    rating = models.IntegerField(default=1)
    duration = models.IntegerField(default=1)
    poster = models.CharField(max_length=200, null=True)
    genres = models.ManyToManyField(Genre)

    def __str__(self):
        return self.title
