from django.urls import path
from . import views

app_name = 'crudFilm'

# base url = film/
urlpatterns = [
    path('', views.apiOverview, name='api=overview'),
    path('film-list/', views.filmList, name='film-list'),
    path('film-detail/<str:pk>/', views.filmDetail, name='film-detail'),
    path('film-create/', views.filmCreate, name='film-create'),
    path('film-update/<str:pk>/', views.filmUpdate, name='film-update'),
    path('film-delete/<str:pk>/', views.filmDelete, name='film-delete'),
    path('genre-list/', views.genreList, name='genre-list'),
]