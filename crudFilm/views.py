from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import FilmSerializer, GenreSerializer
from .models import Film, Genre

# API Overview
@api_view(['GET'])
def apiOverview(request):
    api_urls = {
        'List':'/film-list/',
        'Detail View':'/film-detail/<str:pk>/',
        'Create':'/film-create/',
        'Update':'/film-update/<str:pk>/',
        'Delete':'/film-delete/<str:pk>/',
    }

    return Response(api_urls)

# Api Film
@api_view(['GET'])
def filmList(request):
    films = Film.objects.all()
    serializer = FilmSerializer(films, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def filmDetail(request, pk):
    films = Film.objects.get(id=pk)
    serializer = FilmSerializer(films, many=False)
    return Response(serializer.data)

@api_view(['POST'])
def filmCreate(request, *args, **kwargs):
    data = request.data
    new_film = Film.objects.create(
        title=data['title'],
        desc=data['desc'],
        rating=data['rating'],
        duration=data['duration'],
        poster=data['poster']
    )
    new_film.save()
    
    for genre in data['genres']:
        genre_obj = Genre.objects.get(name=genre['name'])
        new_film.genres.add(genre_obj)
    
    serializer = FilmSerializer(new_film)
    return Response(serializer.data)

@api_view(['POST'])
def filmUpdate(request, pk):
    data = request.data
    film = Film.objects.get(id=pk)

    film.genres.clear()

    for genre in data['genres']:
        genre_obj = Genre.objects.get(name=genre['name'])
        film.genres.add(genre_obj)

    serializer = FilmSerializer(instance=film, data=data)

    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

@api_view(['DELETE'])
def filmDelete(request, pk):
    films = Film.objects.get(id=pk)
    films.delete()

    return Response('Item successfully deleted!')

# API Genre
@api_view(['GET'])
def genreList(request):
    genres = Genre.objects.all()
    serializer = GenreSerializer(genres, many=True)
    return Response(serializer.data)
