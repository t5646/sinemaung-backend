from django.apps import AppConfig


class CrudfilmConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'crudFilm'
