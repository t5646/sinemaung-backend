from django.apps import AppConfig


class CrudJadwalConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'crud_jadwal'
