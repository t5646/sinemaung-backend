from django.db import models
from crudFilm.models import Film
from crud_bioskop.models import Bioskop
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.


class Jadwal(models.Model):
    jam_tayang = models.TimeField(
        auto_now=False, auto_now_add=False)
    bioskop = models.ForeignKey(
        Bioskop, related_name="list_of_jadwal", on_delete=models.CASCADE, null=True)
    studio = models.CharField(max_length=10, null=False)
    harga = models.IntegerField(default=30000)
    total_bangku = models.IntegerField(
        default=50,
        validators=[
            MaxValueValidator(60),
            MinValueValidator(1)
        ])
    film = models.ForeignKey(Film, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.id) + " " + str(self.film) + " studio " + str(self.studio) + " " + str(self.jam_tayang)


class Bangku(models.Model):
    jadwal = models.ForeignKey(
        Jadwal, related_name="daftar_bangku", on_delete=models.CASCADE, null=True)
    nomor = models.CharField(max_length=3)
    isAvailable = models.BooleanField(default=True)

    def __str__(self):
        return self.nomor

    class Meta:
        unique_together = ('jadwal', 'nomor')
