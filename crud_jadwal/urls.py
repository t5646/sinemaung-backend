from django.urls import path
from . import views

app_name = 'crud_jadwal'

# base url = film/
urlpatterns = [
    path('', views.apiOverview, name='jadwal-api=overview'),
    path('get/', views.get_all_jadwal, name='jadwal-list'),
    path('get/<str:pk>/', views.get_jadwal_id, name='jadwal-id'),
    path('create/', views.create_jadwal, name='jadwal-create'),
    path('delete/<str:pk>', views.delete_jadwal, name='jadwal-delete'),
    path('update/<str:pk>', views.update_jadwal, name='jadwal-update'),
    path('bangku/', views.get_all_bangku, name='bangku-list'),
    path('bangku/set-available/<str:pk>/<str:boolean>',
         views.update_bangku, name='bangku-set')
]
