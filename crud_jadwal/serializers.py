from django.db.models import fields
from rest_framework import serializers
from .models import Jadwal, Bangku, Film


class BangkuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bangku
        fields = ['id', 'nomor', 'isAvailable']


class JadwalSerializer(serializers.ModelSerializer):
    daftar_bangku = BangkuSerializer(many=True, required=False)

    class Meta:
        model = Jadwal
        fields = ['id', 'jam_tayang', 'bioskop', 'studio', 'harga',
                  'total_bangku',  'film', 'daftar_bangku', ]
        depth = 1
