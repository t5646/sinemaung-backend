# Generated by Django 3.2.9 on 2021-12-06 07:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crud_jadwal', '0005_auto_20211204_2253'),
    ]

    operations = [
        migrations.AddField(
            model_name='jadwal',
            name='harga',
            field=models.IntegerField(default=30000),
        ),
    ]
