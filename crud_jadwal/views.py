from django.shortcuts import render
from crudFilm.serializers import FilmSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .serializers import *
from .models import *

# Create your views here.


@api_view(['GET'])
def apiOverview(request):
    api_urls = {
        'Get List': '/get/',
        'Get': '/get/<str:pk>/',
        'Create': '/create/',
        'Update': '/update/<str:pk>/',
        'Delete': '/delete/<str:pk>/',
        'Bangku Set Available ': 'bangku/set-available/<str:pk>/<str:boolean>'
    }

    return Response(api_urls)


@api_view(['GET'])
def get_all_jadwal(request):
    jadwals = Jadwal.objects.all()
    serializer = JadwalSerializer(jadwals, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_jadwal_id(request, pk):
    jadwal = Jadwal.objects.get(id=pk)
    serializer = JadwalSerializer(jadwal, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def get_all_bangku(request):
    bangkus = Bangku.objects.all()
    serializer = BangkuSerializer(bangkus, many=True)
    return Response(serializer.data)


@api_view(['PUT'])
def update_bangku(request, pk, boolean):
    bangku = Bangku.objects.get(id=pk)
    data = {
        "nomor": bangku.nomor,
        "isAvailable": True if boolean == 1 else False
    }
    serializer = BangkuSerializer(instance=bangku, data=data, many=False)
    if serializer.is_valid():
        serializer.save()
    else:
        print(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response(serializer.data)


@api_view(['POST'])
def create_jadwal(request):
    def createNomorBangku(i):
        if i < 10:
            nb1 = "A"
        elif i < 20:
            nb1 = "B"
        elif i < 30:
            nb1 = "C"
        elif i < 40:
            nb1 = "D"
        elif i < 50:
            nb1 = "E"
        else:
            nb1 = "F"
        if i < 50:
            nb2 = str((i % 10) + 1)
        else:
            nb2 = str(i-49)
        return nb1 + nb2

    data = request.data

    try:
        film = Film.objects.get(pk=data['film'])
    except:
        return Response(f"Film with id={data['film']} does not exist", status=status.HTTP_404_NOT_FOUND)
    try:
        bioskop = Bioskop.objects.get(pk=data['bioskop'])
    except:
        return Response(f"Bioskop with id={data['bioskop']} does not exist", status=status.HTTP_404_NOT_FOUND)

    new_jadwal = Jadwal.objects.create(
        jam_tayang=data['jam_tayang'],
        harga=data['harga'],
        studio=data['studio'],
        total_bangku=data['total_bangku'],
        film=film,
        bioskop=bioskop
    )

    for i in range(0, int(data['total_bangku'])):
        bangku = Bangku.objects.create(
            jadwal=Jadwal.objects.get(pk=new_jadwal.id),
            nomor=createNomorBangku(i)
        )
        bangku.save()
    new_jadwal.save()

    serializer = JadwalSerializer(new_jadwal)

    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['DELETE'])
def delete_jadwal(request, pk):
    try:
        jadwal = Jadwal.objects.get(id=pk)
        jadwal.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['PUT'])
def update_jadwal(request, pk):
    data = request.data
    data['id'] = pk
    try:
        film = Film.objects.get(pk=data['film'])
    except:
        return Response(f"Film with id={data['film']} does not exist", status=status.HTTP_404_NOT_FOUND)
    try:
        bioskop = Bioskop.objects.get(pk=data['bioskop'])
    except:
        return Response(f"Bioskop with id={data['bioskop']} does not exist", status=status.HTTP_404_NOT_FOUND)

    jadwal = Jadwal.objects.get(id=pk)
    jadwal.film = film
    jadwal.bioskop = bioskop
    data['jumlah_bangku'] = jadwal.total_bangku

    serializer = JadwalSerializer(instance=jadwal, data=data, read_only=False)

    if serializer.is_valid():
        serializer.save()
    else:
        print(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response(serializer.data)
