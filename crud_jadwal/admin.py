from django.contrib import admin
from .models import Jadwal, Bangku

# Register your models here.
admin.site.register(Jadwal)
admin.site.register(Bangku)
