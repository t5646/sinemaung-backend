from django.apps import AppConfig


class CrudBioskopConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'crud_bioskop'
