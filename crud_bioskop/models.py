from django.db import models
from django.utils import timezone
from crudFilm.models import Film
from crudMakanan.models import Makanan
# Create your models here.


class Studio(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name


class Bioskop(models.Model):
    nama = models.CharField(max_length=200)
    alamat = models.CharField(max_length=1000, null=True)
    jam_buka = models.TimeField(
        auto_now=False, auto_now_add=False, default=timezone.now)
    jam_tutup = models.TimeField(
        auto_now=False, auto_now_add=False, default=timezone.now)
    # list jadwal pindah ke serializer
    list_of_film = models.ManyToManyField(Film)
    list_of_studio = models.ManyToManyField(Studio)

    def __str__(self):
        return self.nama
