from rest_framework import serializers
from .models import Bioskop, Studio
from crud_jadwal.serializers import JadwalSerializer


class BioskopSerializer(serializers.ModelSerializer):
    list_of_jadwal = JadwalSerializer(many=True, required=False)

    class Meta:
        model = Bioskop
        fields = '__all__'
        depth = 1


class StudioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Studio
        fields = '__all__'
