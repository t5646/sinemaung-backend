from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .serializers import *
from .models import *

# API Overview
@api_view(['GET'])
def apiOverview(request):
    api_urls = {
        'List':'/get',
        'Detail':'/get/<str:pk>',
        'Create':'/create',
        'Update':'/update/<str:pk>',
        'Delete':'/delete/<str:pk>',
        'Studios':'/get/<str:pk>/studios'
    }

    return Response(api_urls)
    
# Api Bioskop
@api_view(['GET'])
def bioskopList(request):
    bioskops = Bioskop.objects.all()
    serializer = BioskopSerializer(bioskops, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def bioskopDetail(request, pk):
    bioskops = Bioskop.objects.get(id=pk)
    serializer = BioskopSerializer(bioskops, many=False)
    return Response(serializer.data)

@api_view(['POST'])
def bioskopCreate(request, *args, **kwargs):
    
    data = request.data

    new_bioskop = Bioskop.objects.create(
        nama = data['nama'],
        alamat = data['alamat'],
        jam_buka = data['jam_buka'],
        jam_tutup = data['jam_tutup'],
        # list_of_film = data['list_of_film'],
        # list_of_jadwal = data['list_of_jadwal'],
        # list_of_makanan = data['list_of_makanan'],
        # list_of_studio = data['list_of_studio']
    )
    new_bioskop.save()
    stud = data['list_of_studio'].split(",")
    for i in stud:
        new_studio = Studio.objects.create(name=i)
        new_bioskop.list_of_studio.add(new_studio)

    fil = data['list_of_film']
    for i in fil:
        try:
            film = Film.objects.get(id=i)
        except:
            return Response(f"Film with id={i} does not exist", status=status.HTTP_404_NOT_FOUND)
        new_bioskop.list_of_film.add(film)
    
    # for i in data['list_of_jadwal']:
    #     new_bioskop.list_of_jadwal.add(i)

    # for i in data['list_of_makanan']:
    #     new_bioskop.list_of_makanan.add(i)

    serializer = BioskopSerializer(new_bioskop)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['PUT'])
def bioskopUpdate(request, pk):
    data = request.data
    bioskop = Bioskop.objects.get(id=pk)

    studioList = bioskop.list_of_studio.all();
    bioskop.list_of_studio.clear()


    stud = data['list_of_studio']
    if (isinstance(stud, str)):
        stud = stud.split(",")
    for i in stud:
        i.replace(" ","")
        i.replace(",","")
        try:
            new_studio = studioList.get(name=i)
        except:
            new_studio = Studio.objects.create(name=i)
        bioskop.list_of_studio.add(new_studio)

    fil = data['list_of_film']
    for i in fil:
        try:
            film = Film.objects.get(id=i)
        except:
            return Response(f"Film with id={i} does not exist", status=status.HTTP_404_NOT_FOUND)
        bioskop.list_of_film.add(film)

    serializer = BioskopSerializer(instance=bioskop, data=data, read_only=False)

    if serializer.is_valid():
        serializer.save()
    else:
        print(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response(serializer.data)

@api_view(['DELETE'])
def bioskopDelete(request, pk):
    try:
        bioskops = Bioskop.objects.get(id=pk)
        bioskops.delete()

        return Response('Item successfully deleted!', status=status.HTTP_204_NO_CONTENT)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

# API Studio (Per Bioskop)
@api_view(['GET'])
def studioList(request, pk):
    bioskop = Bioskop.objects.get(id=pk)

    serializer = StudioSerializer(bioskop.list_of_studio.all(), many=True)
    return Response(serializer.data)