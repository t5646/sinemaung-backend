from django.urls import path
from . import views

app_name = 'crud_bioskop'

# base url = bioskop/
urlpatterns = [
    path('', views.apiOverview, name='bioskop-api=overview'),
    path('get', views.bioskopList, name='bioskop-list'),
    path('get/<str:pk>', views.bioskopDetail, name='bioskop-detail'),
    path('create', views.bioskopCreate, name='bioskop-create'),
    path('update/<str:pk>', views.bioskopUpdate, name='bioskop-update'),
    path('delete/<str:pk>', views.bioskopDelete, name='bioskop-delete'),
    path('get/<str:pk>/studios', views.studioList, name='studio-list')
]