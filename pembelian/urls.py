from django.urls import path

from . import views

app_name = 'pembelian'

urlpatterns = [
    path('receipt/<str:pk>/', views.pembelianDetail, name='pembelian-detail'),
    path('pembelian-create/', views.pembelianCreate, name='pembelian-create'),
    path('pembelian-update/<str:pk>/', views.pembelianUpdate, name='pembelian-update'),
]