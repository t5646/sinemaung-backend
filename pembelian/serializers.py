from rest_framework import serializers
from .models import Pembelian

class PembelianSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pembelian
        fields = '__all__'
        depth = 1



