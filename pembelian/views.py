from django.shortcuts import render

from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import PembelianSerializer
from .models import Pembelian
from crud_jadwal.models import Bangku
from crudMakanan.models import Makanan



@api_view(['GET'])
def pembelianDetail(request, pk):
    pembelian = Pembelian.objects.get(idReceipt=pk)
    serializerTiket = PembelianSerializer(pembelian, many=False )
    return Response(serializerTiket.data)

@api_view(['POST'])
def pembelianCreate(request, *args, **kwargs):
    data = request.data
    new_pembelian = Pembelian.objects.create(
        hargaTiket=data['totalHarga'],
        totalHarga=data['totalHarga'],
        idReceipt=data['idReceipt'],
        pembeli=data['pembeli']
    )
    new_pembelian.save()

    for tiket in data['tiket']:
        tiket_obj = Bangku.objects.get(id=tiket['id'])
        tiket_obj.isAvailable = False
        tiket_obj.save()
        new_pembelian.tiket.add(tiket_obj)

    serializer = PembelianSerializer(new_pembelian)
    return Response(serializer.data)


@api_view(['POST'])
def pembelianUpdate(request, pk):
    data = request.data
    pembelian = Pembelian.objects.get(idReceipt=pk)
    harga = pembelian.totalHarga
    harga += data['totalHarga']
    pembelian.makanan.clear()
    print(harga)
    pembelian.totalHarga = harga
    pembelian.hargaMakanan=data['totalHarga']
    pembelian.save()

    for makan in data['makanan']:
        makan_obj = Makanan.objects.get(id=makan['id'])
        pembelian.makanan.add(makan_obj)

    serializer = PembelianSerializer(instance=pembelian, data=data)

    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)