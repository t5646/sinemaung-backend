from django.db import models
from crud_jadwal.models import Bangku
from crudMakanan.models import Makanan

# Create your models here.





class Pembelian(models.Model):
    totalHarga = models.IntegerField(default=0)
    hargaMakanan = models.IntegerField(default=0)
    hargaTiket = models.IntegerField(default=0)
    idReceipt = models.CharField(max_length=200)
    pembeli = models.CharField(max_length=100)
    tiket = models.ManyToManyField(Bangku)
    makanan = models.ManyToManyField(Makanan,null=True)

    def __str__(self):
        return self.pembeli

