from rest_framework.authtoken.views import obtain_auth_token
from django.urls import path
from . import views

app_name = 'authentication'


# base url = film/
urlpatterns = [
    path('', views.apiOverview, name='authentication=overview'),
    path('register/', views.register, name='register'),
    path('login/', views.CustomAuthToken.as_view(), name='login'),
    # path('get/', views.get_all, name='get')
]
