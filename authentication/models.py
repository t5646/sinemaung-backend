from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from rest_framework.authtoken.models import Token


class MyAccountManager(BaseUserManager):
    def create_user(self, username, role='', password=None):
        if not username:
            raise ValueError("Users must have a username")

        user = self.model(
            username=username,
            role=role
        )
        user.is_active = True
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password=None, role='',):
        if not username:
            raise ValueError("Users must have a username")

        user = self.create_user(
            username=username,
            password=password,
            role=role
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class UserAccount(AbstractBaseUser):
    username = models.CharField(
        verbose_name="username", max_length=50, unique=True)
    role = models.CharField(max_length=10, blank=True, default='')
    date_joined = models.DateTimeField(
        verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(
        verbose_name='last login', null=True, auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'

    objects = MyAccountManager()

    def __str__(self):
        return self.username + " " + self.role

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def _post_save_receiver(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
