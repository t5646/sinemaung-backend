from django.shortcuts import render
from crudFilm.serializers import FilmSerializer
from rest_framework.decorators import api_view
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from .serializers import *
from .models import *


# Create your views here.
@api_view(['GET'])
def apiOverview(request):
    api_urls = {
        'Register': 'register/',
        'Login': 'login/',
    }

    return Response(api_urls)


@api_view(['POST'])
def register(request):
    serializer = RegistrationSerializer(data=request.data)
    data = {}
    if serializer.is_valid():
        user = serializer.save()
        data['username'] = user.username
        data['role'] = user.role
        data['token'] = Token.objects.get(user=user).key
    else:
        data = serializer.errors
    return Response(data)


@api_view(['POST'])
def login(request):
    return Response()


@api_view(['GET'])
def get_all(request):
    jadwals = UserAccount.objects.all()
    serializer = AccountSerializer(jadwals, many=True)
    return Response(serializer.data)


class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'username': user.username,
            'role': user.role,
        })
