from django.urls import path
from . import views

app_name = 'crudFilm'

urlpatterns = [
    path('', views.apiOverview, name='api=overview'),
    path('makanan-list/', views.makananList, name='makanan-list'),
    path('makanan-create/', views.makananCreate, name='makanan-create'),
    path('makanan-update/<str:pk>/', views.makananUpdate, name='makanan-update'),
    path('makanan-detail/<str:pk>/', views.makananDetail, name='makanan-detail'),
    path('makanan-delete/<str:pk>/', views.makananDelete, name='makanan-delete')
]