from django.db import models

# Create your models here.
class Makanan(models.Model):
    nama = models.CharField(max_length=200)
    desc = models.CharField(max_length=1000)
    harga = models.IntegerField()
    gambar = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.nama
