from django.shortcuts import render
from crudMakanan.serializers import MakananSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .serializers import *
from .models import *
# Create your views here.

@api_view(['GET'])
def apiOverview(request):
    api_urls = {
        'List':'/makanan-list/',
        'Create':'/makanan-create/',
        'Update':'/makanan-update/<str:pk>/',
        'Detail':'/makanan-detail/<str:pk>/',
        'Delete':'/makanan-delete/<str:pk>/',
    }

    return Response(api_urls)

@api_view(['GET'])
def makananList(request):
    makanan = Makanan.objects.all()
    serializer = MakananSerializer(makanan, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def makananDetail(request, pk):
    makanan = Makanan.objects.get(id=pk)
    serializer = MakananSerializer(makanan, many=False)
    return Response(serializer.data)

@api_view(['POST'])
def makananCreate(request):
    data = request.data
    new_makanan = Makanan.objects.create(
        nama=data['nama'],
        desc=data['desc'],
        harga=data['harga'],
        gambar=data['gambar']
    )
    new_makanan.save()
    serializer = MakananSerializer(new_makanan)
    return Response(serializer.data)

@api_view(['PUT'])
def makananUpdate(request, pk):
    makanan = Makanan.objects.get(id=pk)
    serializer = MakananSerializer(instance=makanan, data=request.data)

    if serializer.is_valid():
        serializer.save()


    return Response(serializer.data)

@api_view(['DELETE'])
def makananDelete(request, pk):
    makanan = Makanan.objects.get(id=pk)
    makanan.delete()

    return Response("Makanan berhasil dihapus!")