# Kelompok 11-B Sinemaung

## Anggota Kelompok

- Aimar Fikri Salafi - 1906400186
- Haidar Labib Bangsawijaya - 1906400135
- Johanes Jason. - 1906293120
- Naufal Adi Wijanarko - 1906305871
- Ryanda Mahdy Ananda - 1906351064

## URL

### Film

- list film: http://localhost:8000/film/film-list/
- detail: http://localhost:8000/film/film-detail/:id
- create: http://localhost:8000/film/film-create/
- update: http://localhost:8000/film/film-update/:id/
- delete: http://localhost:8000/film/film-delete/:id/
- list genre: http://localhost:8000/film/genre-list/

## Jadwal

- Get All: /get/
- Get: /get/<str:pk>/
- Create: /create/
- Update: /update/<str:pk>/
- Delete: /delete/<str:pk>/
- Bangku/Set Available: /bangku/set-available/<str:pk>/<str:boolean>

## Makanan
- list makanan: http://localhost:8000/makanan/makanan-list/
- create: http://localhost:8000/makanan/makanan-create/
- update: http://localhost:8000/makanan/makanan-update/:id/
- delete: http://localhost:8000/makanan/makanan-delete/:id/
- detail: http://localhost:8000/makanan/makanan-detail/:id/

## Pembelian
- get receipt : http://localhost:8000/membeli/receipt/<str:pk>/
- create pembelian : http://localhost:8000/membeli/pembelian-create/
- update pembelian : http://localhost:8000/membeli/pembelian-update/<str:pk>/
